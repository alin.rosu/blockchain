package com.rap.blockchain.server

import cats.*
import cats.effect.{Async, Sync}
import cats.implicits.*
import com.rap.blockchain.algebra.BlockchainAlgebra
import com.rap.blockchain.config.AppConfig
import com.rap.common.logging.Logging
import fs2.io.net.Network
import org.http4s.ember.server.EmberServerBuilder

import java.time.ZonedDateTime

object BlockchainServer extends Logging:
  def run[F[_]: Async]: F[Unit] = for {
    logger            <- logger
    config            <- AppConfig[F].load
    _                 <- logger.info("Config loaded")
    initialBlockTS    <- Sync[F].delay(ZonedDateTime.now())
    blockchainAlgebra <- BlockchainAlgebra[F](config.maxAttempts, initialBlockTS)
    httpApp = BlockchainRoutes[F](blockchainAlgebra).orNotFound
    given Network[F] <- Applicative[F].pure(Network.forAsync[F])
    _ <- EmberServerBuilder.default
      .withHost(config.serverHost)
      .withPort(config.serverPort)
      .withHttpApp(httpApp)
      .build
      .useForever
  } yield ()
