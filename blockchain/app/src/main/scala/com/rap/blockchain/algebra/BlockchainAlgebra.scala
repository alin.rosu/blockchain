package com.rap.blockchain.algebra

import cats.effect.std.SecureRandom
import cats.effect.{Ref, Sync}
import cats.implicits.*
import com.rap.blockchain.hash.SHA256Hash
import com.rap.blockchain.model.Block
import com.rap.blockchain.model.Block.Proof

import java.time.ZonedDateTime
import scala.annotation.tailrec

trait BlockchainAlgebra[F[_]](val block: Ref[F, Block]) {
  def append(proof: Proof): F[Block]
  def proof(): F[Option[Proof]]
  def check(): F[Boolean]
  def linearize(): F[List[Block]]
}

object BlockchainAlgebra:
  def apply[F[_]: Sync](maxAttempts: Int, initialBlockTS: ZonedDateTime): F[BlockchainAlgebra[F]] =
    Ref.of(Block(0, initialBlockTS, Proof(1), None, None)).map(ref =>
      new BlockchainAlgebra[F](ref):
        def append(proof: Proof): F[Block] =
          for {
            block <- ref.get
            ts    <- Sync[F].delay(ZonedDateTime.now())
            newBlock = Block(block.index + 1, ts, proof, Some(block.sha256()), Some(block))
            _ <- ref.set(newBlock)
          } yield newBlock

        def proof(): F[Option[Proof]] =
          def recurse(previousProof: Proof, currentProof: Proof, attempt: Int): F[Option[Proof]] =
            if (attempt == 0)
              None.pure[F]
            else if (checkProofs(previousProof, currentProof))
              Some(currentProof).pure[F]
            else
              SecureRandom.javaSecuritySecureRandom.flatMap(_.nextInt).flatMap(i =>
                recurse(previousProof, Proof(BigInt(i)), attempt - 1)
              )
          ref.get.flatMap(block => recurse(block.proof, Proof(BigInt(1)), maxAttempts))

        def check(): F[Boolean] =
          @tailrec
          def recurse(block: Block): Boolean =
            (block.previousHash, block.previousBlock) match
              case (Some(h), Some(previous))
                  if previous.sha256() == h && checkProofs(previous.proof, block.proof) => recurse(previous)
              case (None, None) => block.index == 0
              case _            => false

          ref.get.map(recurse)

        def linearize(): F[List[Block]] =
          @tailrec
          def recurse(block: Block, acc: List[Block] = List()): List[Block] =
            block.previousBlock match {
              case None           => block :: acc
              case Some(previous) => recurse(previous, block :: acc)
            }
          ref.get.map(block => recurse(block))

        private def checkProofs(previous: Proof, current: Proof): Boolean =
          val h = SHA256Hash((current.value.pow(2) - previous.value.pow(2)).toString())
          h.value.startsWith("0000")
    )
