package com.rap.blockchain.hash

import java.nio.charset.StandardCharsets
import java.security.MessageDigest

case class SHA256Hash private (value: String)

object SHA256Hash {
  def apply(s: String): SHA256Hash =
    val digest = MessageDigest.getInstance("SHA-256")
    val bytes  = digest.digest(s.getBytes(StandardCharsets.UTF_8))
    new SHA256Hash(bytes.map(b => Integer.toHexString(0xff & b)).flatMap(hex =>
      if (hex.length() == 1) List("0", hex) else List(hex)
    ).reduce(_ + _))
}
