package com.rap.blockchain.model

import cats.implicits.*
import com.rap.blockchain.hash.SHA256Hash
import com.rap.blockchain.model.Block.Proof
import com.rap.common.`type`.NewType
import io.circe.syntax.*
import io.circe.{Encoder, Json}

import java.time.ZonedDateTime

final case class Block(
  index: BigInt,
  timestamp: ZonedDateTime,
  proof: Proof,
  previousHash: Option[SHA256Hash],
  previousBlock: Option[Block]
) {
  def sha256(): SHA256Hash = SHA256Hash(this.asJson.toString)
}

object Block {
  type Proof = Proof.Type
  object Proof extends NewType[BigInt]

  given Encoder[Block] = block => {
    import block.*
    Json.obj(
      "index" -> Json.fromBigInt(index),
      "timestamp" -> Json.fromString(timestamp.toString),
      "proof" -> Json.fromBigInt(proof.value),
      "previousHash" -> previousHash.map(hash => Json.fromString(hash.value)).getOrElse(Json.Null)
    ).deepDropNullValues
  }
}
