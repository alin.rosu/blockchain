package com.rap.blackchain

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.rap.blockchain.algebra.BlockchainAlgebra
import com.rap.blockchain.model.Block
import com.rap.blockchain.model.Block.Proof
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.time.ZonedDateTime

class BlockchainAlgebraSpec extends AnyFlatSpec with Matchers {

  "BlockchainAlgebra.append" should "add a new block with the given proof" in {
    val initialTime = ZonedDateTime.parse("2024-01-01T11:00:00Z")

    val firstBlock        = Block(0, initialTime, Proof(1), None, None)
    val blockchainAlgebra = BlockchainAlgebra[IO](10, initialTime)
    val proof             = Proof(BigInt(200))

    blockchainAlgebra
      .flatMap(a => a.append(proof))
      .map(newBlock => {
        val _ = newBlock.index shouldBe 1
        val _ = newBlock.proof shouldBe proof
        newBlock.previousHash shouldBe Some(firstBlock.sha256())
      }).unsafeRunSync()
  }

  "BlockchainAlgebra.check" should "return true for a valid blockchain" in {
    val initialTime = ZonedDateTime.parse("2024-01-01T12:00:00Z")
    val blockchainAlgebra = BlockchainAlgebra[IO](10, initialTime).unsafeRunSync()

    val isValid = blockchainAlgebra.check().unsafeRunSync()

    isValid shouldBe true
  }

}
