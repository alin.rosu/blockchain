package com.rap.blackchain

import com.rap.blockchain.model.*
import com.rap.blockchain.model.Block.Proof
import io.circe.syntax.*
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.time.ZonedDateTime

class BlockSpec extends AnyFlatSpec with Matchers {

  "A Block" should "serialize to JSON correctly" in {
    val firstBlock = Block(0, ZonedDateTime.parse("2024-01-01T11:00Z"), Proof(1), None, None)

    // Sample data
    val block = Block(
      index         = BigInt(1),
      timestamp     = ZonedDateTime.parse("2024-01-01T12:00Z"),
      proof         = Proof(BigInt(100)),
      previousHash  = Some(firstBlock.sha256()),
      previousBlock = None
    )

    // Serialize the block to JSON
    val json = block.asJson.noSpaces

    // Expected JSON string
    val expectedJson = s"""{
      "index":1,
      "timestamp":"2024-01-01T12:00Z",
      "proof":100,
      "previousHash":"${firstBlock.sha256().value}"
    }""".stripMargin.replaceAll("\n", "").replaceAll(" ", "")

    json shouldEqual expectedJson
  }
}
